#include<iostream>
using namespace std;
int main()
{
     int secret = 7;
     int guess_limit = 4;
     int num;
     int count = 1;

     cout<<"Please guess the secret number:"<<endl;
     cin>>num;

     while (num != secret && count < guess_limit)
     {
         cout<<count<<endl;
         count = count +1;
      	 cout<<"Please guess the secret number:"<<endl;
         cin>>num;
     }

     if (count >= guess_limit)
     {
       cout<<"Sorry you have lost the game"<<endl;
     }
     else
     {
       cout<<"You have won this game"<<endl;
     }
     return 0;
 }

